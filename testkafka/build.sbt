name := """TestKafka"""
organization := "com.nCinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  "log4j" % "log4j" % "1.2.17",
  "org.apache.kafka" %% "kafka" % "2.4.1"

)
