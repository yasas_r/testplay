package kafka;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;

import java.util.Properties;

public class ProducerOne {

    private static Logger logger = Logger.getLogger(ConsumerOne.class);

    private ProducerOne() {
    }

    public static void sendMessageSync(String topic, String message) {

        String bootstrapServers = "localhost:9092,localhost:9093";

        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        ;

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        ProducerRecord<String, String> record = new ProducerRecord<>(topic, message);
        try {
            producer.send(record);
        } catch (Exception e) {
            logger.error("Error sending: " + e.getMessage());
            e.printStackTrace();
        }
        producer.flush();
        producer.close();
    }

    public static void sendMessageAsync(String topic, String message) {

        String bootstrapServers = "localhost:9092,localhost:9093";

        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, message);

        producer.send(record, (metadata, exception) -> {
            if (metadata != null) {
                System.out.printf("sent record(key=%s value=%s) meta(partition=%d, offset=%d)\n",
                        record.key(), record.value(), metadata.partition(), metadata.offset());
            } else {
                logger.error("Error sending: " + exception.getMessage());
                exception.printStackTrace();
            }
        });

        producer.flush();
        producer.close();
    }

}

class DemoProducerCallback implements Callback {
    @Override
    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
        if (e != null) {
            e.printStackTrace();
        }
    }
}


