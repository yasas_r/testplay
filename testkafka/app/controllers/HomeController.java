package controllers;

import kafka.ConsumerOne;
import kafka.ProducerOne;
import play.mvc.*;

public class HomeController extends Controller {


    public Result index() {

        return ok(views.html.index.render());
    }

    public Result produce() {

        String topic="my_first";
        String message1="first_message";
        ProducerOne.sendMessageSync(topic,message1);

        String message2="second_message";
        ProducerOne.sendMessageSync(topic,message2);

        ProducerOne.sendMessageSync(topic,"message2");

        return ok("Produced");
    }

    public Result consume() {

        String grp_id="third_app";
        String topic="my_first";
        ConsumerOne.readMessage(grp_id,topic);

        return ok("Consuming");
    }

}
